Source: python-elasticsearch
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
 Anthony Fok <foka@debian.org>,
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-elasticsearch-transport,
 python3-setuptools,
Build-Depends-Indep:
 libjs-jquery,
 python3-coverage,
 python3-pylibmc,
 python3-requests,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
 python3-urllib3,
 python3-yaml,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-elasticsearch
Vcs-Git: https://salsa.debian.org/python-team/packages/python-elasticsearch.git
Homepage: https://github.com/elasticsearch/elasticsearch-py
Rules-Requires-Root: no
X-Style: black

Package: python3-elasticsearch
Architecture: all
Depends:
 python3-elasticsearch-transport,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-elasticsearch-doc,
Description: Python client for Elasticsearch (Python3 version)
 Official low-level client for Elasticsearch. Its goal is to provide common
 ground for all Elasticsearch-related code in Python; because of this it tries
 to be opinion-free and very extendable.
 .
 The client's features include:
  * translating basic Python data types to and from json (datetimes are not
    decoded for performance reasons)
  * configurable automatic discovery of cluster nodes
  * persistent connections
  * load balancing (with pluggable selection strategy) across all available
    nodes
  * failed connection penalization (time based - failed connections won't be
    retried until a timeout is reached)
  * thread safety
  * pluggable architecture
 .
 This package contains the Python 3 version of the library.

Package: python-elasticsearch-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Python client for Elasticsearch (Documentation)
 Official low-level client for Elasticsearch. Its goal is to provide common
 ground for all Elasticsearch-related code in Python; because of this it tries
 to be opinion-free and very extendable.
 .
 The client's features include:
  * translating basic Python data types to and from json (datetimes are not
    decoded for performance reasons)
  * configurable automatic discovery of cluster nodes
  * persistent connections
  * load balancing (with pluggable selection strategy) across all available
    nodes
  * failed connection penalization (time based - failed connections won't be
    retried until a timeout is reached)
  * thread safety
  * pluggable architecture
 .
 This package contains the documentation.
